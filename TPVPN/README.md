# **TP VPN**
Vérification de la config ip : 
```
[thomas@vpn ovpn]$ ip a
...
4: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 100
    link/none
    inet 10.8.0.1 peer 10.8.0.2/32 scope global tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::5f23:3da4:2509:266a/64 scope link flags 800
       valid_lft forever preferred_lft forever
```
Vérification du status du service openVPN
```
[thomas@vpn ovpn]$ sudo systemctl status openvpn@cesi_vpn
● openvpn@cesi_vpn.service - OpenVPN Robust And Highly Flexible Tunneling Application On cesi_vpn
   Loaded: loaded (/usr/lib/systemd/system/openvpn@.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-01-07 15:23:24 CET; 2min 25s ago
```

Test de connexion depuis une autre VM : 
```
[thomas@db openvpn]$ sudo openvpn cesi_client.ovpn
Thu Jan  7 16:09:08 2021 OpenVPN 2.4.10 x86_64-redhat-linux-gnu [Fedora EPEL patched] [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Dec  9 2020
Thu Jan  7 16:09:08 2021 library versions: OpenSSL 1.0.2k-fips  26 Jan 2017, LZO 2.06
Thu Jan  7 16:09:08 2021 Outgoing Control Channel Authentication: Using 256 bit message hash 'SHA256' for HMAC authentication
Thu Jan  7 16:09:08 2021 Incoming Control Channel Authentication: Using 256 bit message hash 'SHA256' for HMAC authentication
Thu Jan  7 16:09:08 2021 TCP/UDP: Preserving recently used remote address: [AF_INET]10.99.99.14:1194
Thu Jan  7 16:09:08 2021 Socket Buffers: R=[212992->212992] S=[212992->212992]
Thu Jan  7 16:09:08 2021 UDP link local: (not bound)
Thu Jan  7 16:09:08 2021 UDP link remote: [AF_INET]10.99.99.14:1194
Thu Jan  7 16:09:08 2021 TLS: Initial packet from [AF_INET]10.99.99.14:1194, sid=8b8538b8 000e3aeb
Thu Jan  7 16:09:08 2021 VERIFY OK: depth=1, CN=vpn.cesi
Thu Jan  7 16:09:08 2021 VERIFY KU OK
Thu Jan  7 16:09:08 2021 Validating certificate extended key usage
Thu Jan  7 16:09:08 2021 ++ Certificate has EKU (str) TLS Web Server Authentication, expects TLS Web Server Authentication
Thu Jan  7 16:09:08 2021 VERIFY EKU OK
Thu Jan  7 16:09:08 2021 VERIFY OK: depth=0, CN=cesi
Thu Jan  7 16:09:08 2021 Control Channel: TLSv1.2, cipher TLSv1/SSLv3 ECDHE-RSA-AES256-GCM-SHA384, 2048 bit RSA
Thu Jan  7 16:09:08 2021 [cesi] Peer Connection Initiated with [AF_INET]10.99.99.14:1194
Thu Jan  7 16:09:09 2021 SENT CONTROL [cesi]: 'PUSH_REQUEST' (status=1)
Thu Jan  7 16:09:09 2021 PUSH: Received control message: 'PUSH_REPLY,redirect-gateway def1 bypass-dhcp,dhcp-option DNS 208.67.222.222,dhcp-option DNS 208.67.220.220,route 10.8.0.1,topology net30,ping 10,ping-restart 120,ifconfig 10.8.0.6 10.8.0.5,peer-id 0,cipher AES-256-GCM'
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: timers and/or timeouts modified
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: --ifconfig/up options modified
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: route options modified
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: --ip-win32 and/or --dhcp-option options modified
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: peer-id set
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: adjusting link_mtu to 1624
Thu Jan  7 16:09:09 2021 OPTIONS IMPORT: data channel crypto options modified
Thu Jan  7 16:09:09 2021 Data Channel: using negotiated cipher 'AES-256-GCM'
Thu Jan  7 16:09:09 2021 Outgoing Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
Thu Jan  7 16:09:09 2021 Incoming Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
Thu Jan  7 16:09:09 2021 ROUTE_GATEWAY 192.168.81.2/255.255.255.0 IFACE=ens33 HWADDR=00:0c:29:33:47:38
Thu Jan  7 16:09:09 2021 TUN/TAP device tun0 opened
Thu Jan  7 16:09:09 2021 TUN/TAP TX queue length set to 100
Thu Jan  7 16:09:09 2021 /sbin/ip link set dev tun0 up mtu 1500
Thu Jan  7 16:09:09 2021 /sbin/ip addr add dev tun0 local 10.8.0.6 peer 10.8.0.5
Thu Jan  7 16:09:09 2021 /sbin/ip route add 10.99.99.14/32 via 192.168.81.2
Thu Jan  7 16:09:09 2021 /sbin/ip route add 0.0.0.0/1 via 10.8.0.5
Thu Jan  7 16:09:09 2021 /sbin/ip route add 128.0.0.0/1 via 10.8.0.5
Thu Jan  7 16:09:09 2021 /sbin/ip route add 10.8.0.1/32 via 10.8.0.5
Thu Jan  7 16:09:09 2021 WARNING: this configuration may cache passwords in memory -- use the auth-nocache option to prevent this
Thu Jan  7 16:09:09 2021 Initialization Sequence Completed
```
