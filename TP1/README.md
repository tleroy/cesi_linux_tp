# **TP1 Thomas Leroy**

## I. Utilisateurs

### 1. Création et configuration
---
Création d'un utilisateur `thomas` et du groupe `admins` :
```
sudo useradd thomas -m -s /bin/bash

sudo groupadd admins
```
Ajout de tous les droites au groupe `admins` :
```
sudo visudo -f /etc/sudoers

## Allows people in group admins to run all commands
%admins ALL=(ALL)       ALL
```
Ajout de l'utilisateur `thomas` au groupe `admins` :
```
sudo usermod -aG admins thomas
```

## II. Configuration réseau

### 1. Nom de domaine
---
Définition du nom de domaine `vm1.tp1` :
```
echo 'vm1.tp1' | sudo tee /etc/hostname
```
### 2. Serveur DNS

Définition du serveur DNS `1.1.1.1` :
 ```
BOOTPROTO=static
NAME=ens37
DEVICE=ens37
ONBOOT=yes
IPADDR=192.168.184.10
NETMASK=255.255.255.0
DNS1=1.1.1.1
```
```
[thomas@vm1 ~]$ dig google.com

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5059
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; MBZ: 0x0005, udp: 1472
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             5       IN      A       216.58.201.238

;; Query time: 13 msec
;; SERVER: 192.168.81.2#53(192.168.81.2)
;; WHEN: Tue Jan 05 17:01:36 CET 2021
;; MSG SIZE  rcvd: 55
```

## III. Partitionnement

### 2. Partitionnement
---
Création des 3 *Logical Volume* : 
```
[thomas@vm1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[thomas@vm1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.

[thomas@vm1 ~]$ sudo pvs
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <19.00g     0
  /dev/sdb   data   lvm2 a--   <3.00g <3.00g
  /dev/sdc   data   lvm2 a--   <3.00g <3.00g

[thomas@vm1 ~]$ sudo vgcreate data /dev/sdb /dev/sdc
  Volume group "data" successfully created

[thomas@vm1 ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   2   0 wz--n- <19.00g    0
  data     2   0   0 wz--n-   5.99g 5.99g


[thomas@vm1 ~]$ sudo lvcreate -L 2Go data -n data1
  Logical volume "data1" created.
[thomas@vm1 ~]$ sudo lvcreate -L 2Go data -n data2
  Logical volume "data2" created. 
[thomas@vm1 ~]$ sudo lvcreate -l 100%FREE data -n data3
  Logical volume "data3" created. 

[thomas@vm1 ~]$ sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root  centos -wi-ao---- <17.00g
  swap  centos -wi-ao----   2.00g
  data1 data   -wi-a-----   2.00g
  data2 data   -wi-a-----   2.00g
  data3 data   -wi-a-----   1.99g
```
Formatage des 3 partitions en `ext4` :
```
sudo mkfs -t ext4 /dev/data/data1
sudo mkfs -t ext4 /dev/data/data2
sudo mkfs -t ext4 /dev/data/data3
```
Montage des partitions :
```
[thomas@vm1 mnt]$ sudo mkdir part1 part2 part3

 sudo mount /dev/data/data1 /mnt/part1
 sudo mount /dev/data/data2 /mnt/part2
 sudo mount /dev/data/data3 /mnt/part3
```
Modification du fichier `/etc/fstab` afin que les partitions soit montée automatiquement au démarage :
```
sudo vim /etc/fstab

#data1
/dev/data/data1 /mnt/part1      ext4    defaults        0       0
#data2
/dev/data/data2 /mnt/part2      ext4    defaults        0       0
#data3
/dev/data/data3 /mnt/part3      ext4    defaults        0       0



[thomas@vm1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
swap                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
/mnt/part2               : already mounted
/mnt/part3               : already mounted



[thomas@vm1 ~]$ df
Filesystem              1K-blocks    Used Available Use% Mounted on
devtmpfs                   485940       0    485940   0% /dev
tmpfs                      497836       0    497836   0% /dev/shm
tmpfs                      497836    7832    490004   2% /run
tmpfs                      497836       0    497836   0% /sys/fs/cgroup
/dev/mapper/centos-root  17811456 1555076  16256380   9% /
/dev/sda1                 1038336  198616    839720  20% /boot
/dev/mapper/data-data3    2023376    6120   1896424   1% /mnt/part3
/dev/mapper/data-data2    1998672    6144   1871288   1% /mnt/part2
tmpfs                       99568       0     99568   0% /run/user/1000
/dev/mapper/data-data1    1998672    6144   1871288   1% /mnt/part1
```

## IV. Gestion de services
### 1. Interaction avec un service existant
---
Vérification que le service `firewalld` est démarré et activé :
```
[thomas@vm1 mnt]$ systemctl is-active firewalld
active

[thomas@vm1 mnt]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service
1. Unité simpliste

Ouverture du port 8888 pour le `web.service` :
```
[thomas@vm1 system]$ sudo firewall-cmd --add-port=8888/tcp
success
[thomas@vm1 system]$ sudo firewall-cmd --reload
success
```
Test accès au serveur web :
```
[thomas@vm1 system]$ curl 192.168.184.10:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
```

2. Modification de l'unité

Création de l'utilisateur `web` et du dossier site : 
```
sudo useradd web

sudo mkdir /srv/site
```

Modification du service `web.service` :
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory= /srv/site

[Install]
WantedBy=multi-user.target
```

Ajout d'un fichier `home.htlm` et test de connexion au service : 
```
sudo vim /srv/site/home.html

[thomas@vm1 site]$ sudo chown web home.html
[thomas@vm1 site]$ sudo chgrp web home.html
[thomas@vm1 site]$ ls -al
total 4
drwxr-xr-x. 2 root root  23 Jan  5 15:45 .
drwxr-xr-x. 3 root root  18 Jan  5 15:38 ..
-rw-r--r--. 1 web  web  150 Jan  5 15:44 home.html

[thomas@vm1 srv]$ sudo chown web site/
[thomas@vm1 srv]$ sudo chgrp web site/
[thomas@vm1 srv]$ ls -al
total 0
drwxr-xr-x.  3 root root  18 Jan  5 15:38 .
dr-xr-xr-x. 17 root root 224 Jan  4 16:04 ..
drwxr-xr-x.  2 web  web   23 Jan  5 15:45 site
```

```
[thomas@vm1 ~]$ curl 192.168.184.10:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="home.html">home.html</a>
</ul>
<hr>
</body>
</html>
```