# **TP3**
## I. DNS
### 2. Mise en place 
--- 
Ajout des ports 53 udp et tcp au firewall : 
```
[thomas@ns1 ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[thomas@ns1 ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
success
[thomas@ns1 ~]$ sudo firewall-cmd --reload
success
```
Demarrage du service bind : 
```
[thomas@ns1 ~]$ sudo systemctl start named
```

### 3. Test 
```
[thomas@rp ~]$ dig web.tp3.cesi @10.99.99.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> web.tp3.cesi @10.99.99.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11958
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.tp3.cesi.                  IN      A

;; ANSWER SECTION:
web.tp3.cesi.           604800  IN      A       10.99.99.11

;; AUTHORITY SECTION:
tp3.cesi.               604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.99.99.14

;; Query time: 2 msec
;; SERVER: 10.99.99.14#53(10.99.99.14)
;; WHEN: Fri Jan 08 11:25:15 CET 2021
;; MSG SIZE  rcvd: 91
```
```
[thomas@db ~]$ dig -x 10.99.99.13 @10.99.99.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> -x 10.99.99.13 @10.99.99.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39262
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;13.99.99.10.in-addr.arpa.      IN      PTR

;; ANSWER SECTION:
13.99.99.10.in-addr.arpa. 604800 IN     PTR     rp.tp3.cesi.

;; AUTHORITY SECTION:
99.99.10.in-addr.arpa.  604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.99.99.14

;; Query time: 1 msec
;; SERVER: 10.99.99.14#53(10.99.99.14)
;; WHEN: Fri Jan 08 11:26:04 CET 2021
;; MSG SIZE  rcvd: 112
```

## II. Backup

Le fichier `backup.service` :
```bash
#!/bin/bash

dir_to_backup='/var/www/'
destination='/opt/backup'


backup () {
  # Lance la sauvegarde de $dir_to_backup vers $destination
  # Commande rsync ou borg

    borg create ${destination}::{hostname}_{now} $dir_to_backup
}

rotate () {
  # Garde uniquement les 7 sauvegardes les plus récentes    

    borg prune -v --list -H 10 -d 7 $destination
}

backup
rotate
```

Le fichier `backup.timer` :

```bash
[Unit]
Description=Run backup.service periodically

[Timer]
# Everyday at 15:40
OnCalendar=*-*-* 15:40:00

[Install]
WantedBy=multi-user.target
```

Le résultat après l'exécution du service à 15h40 : 

```
[thomas@web opt]$ sudo borg list backup
[sudo] password for thomas:
web_2021-01-08T12:53:49              Fri, 2021-01-08 12:53:49 [9184aa7bb5709aefe1610c28fb9f1bc6e4eb4f580f09a2ca59ba026c2242360d]
web_2021-01-08T15:40:01              Fri, 2021-01-08 15:40:01 [00d2a298260ace49aeec8f4df9f2bf7047f6c8057117c08b41e36d738d29d2dc]
```