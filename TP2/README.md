# **TP2**

## I. Base de données
---
Installation du paquet `mariadb-server` et démarrage du service :
```
sudo yum install mariadb-server mariadb

sudo systemctl enable mariadb --now
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service.

sudo systemctl start mariadb.service
```
Installation de la base de donnée de manière sécurisée : 
```
sudo mysql_secure_installation
...
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
Création d'un utilisateur et attribution de tous les droits sur la base `wordpress` : 
```
GRANT ALL ON wordpress.* to wordpressuser@10.99.99.11 IDENTIFIED BY '123';
Query OK, 0 rows affected (0.00 sec)
```
Ouverture du port 3306 pour avoir accès à la base : 
```
sudo firewall-cmd --add-port=3306/tcp --permanent
success
```

Test de connexion depuis le serveur web : 
```
mysql -u wordpressuser -p -h 10.99.99.12 -P 3306
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 24
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE DATABASE wordpress;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| wordpress          |
+--------------------+
4 rows in set (0.00 sec)
```


## II. Serveur Web
---
Installation et démarrage service Apache `httpd` :
```
sudo yum install httpd

sudo systemctl enable httpd
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.

sudo systemctl start httpd
```
Installation de WordPress : 
```bash
#Téléchargement du paquet 
wget http://wordpress.org/latest.tar.gz

#Extraction de l'archive
tar xzvf latest.tar.gz

#Transfert des fichiers avec rsync pour préserver les autorisations
sudo rsync -avP ~/wordpress/ /var/www/html/

#Changement de propriétaire des fichiers
sudo chown -R apache:apache /var/www/html/*
```
Copie du fichier sample de configuration pour créer la notre : 
```
cd /var/www/html

cp wp-config-sample.php wp-config.php

vim wp-config.php

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', '10.99.99.12' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
```
Ouverture du port 80 sur le serveur web : 
```
sudo firewall-cmd --add-port=80/tcp --permanent
```

Test de connexion depuis la machine serveur web : 
```html
[thomas@web html]$ curl -L http://localhost:80
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title>WordPress &rsaquo; Installation</title>
        <link rel='stylesheet' id='dashicons-css'  href='http://localhost/wp-includes/css/dashicons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='buttons-css'  href='http://localhost/wp-includes/css/buttons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='forms-css'  href='http://localhost/wp-admin/css/forms.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='l10n-css'  href='http://localhost/wp-admin/css/l10n.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='install-css'  href='http://localhost/wp-admin/css/install.min.css?ver=5.6' type='text/css' media='all' />
</head>
<body class="wp-core-ui language-chooser">
<p id="logo">WordPress</p>

        <form id="setup" method="post" action="?step=1"><label class='screen-reader-text' for='language'>Select a default language</label>
<select size='14' name='language' id='language'>
<option value="" lang="en" selected="selected" data-continue="Continue" data-installed="1">English (United States)</option>
<option value="af" lang="af" data-continue="Gaan voort">Afrikaans</option>
<option value="ar" lang="ar" data-continue="المتابعة">العربية</option>
<option value="ary" lang="ar" data-continue="المتابعة">العربية المغربية</option>
<option value="as" lang="as" data-continue="Continue">অসমীয়া</option>
...
```

## III. Reverse Proxy
--- 
Après l'installation des paquets `epel-release` et `nginx` démarrage du service et ouverture du port 80 : 
 ```
systemctl start nginx && systemctl enable nginx

firewall-cmd --permanent --zone=public --add-service=http
```

Test de connexion depuis l'ip du `reverse proxy` : 
```html
[thomas@web html]$ curl -L http://10.99.99.13:80
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title>WordPress &rsaquo; Installation</title>
        <link rel='stylesheet' id='dashicons-css'  href='http://10.99.99.11/wp-includes/css/dashicons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='buttons-css'  href='http://10.99.99.11/wp-includes/css/buttons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='forms-css'  href='http://10.99.99.11/wp-admin/css/forms.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='l10n-css'  href='http://10.99.99.11/wp-admin/css/l10n.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='install-css'  href='http://10.99.99.11/wp-admin/css/install.min.css?ver=5.6' type='text/css' media='all' />
</head>
<body class="wp-core-ui language-chooser">
<p id="logo">WordPress</p>

        <form id="setup" method="post" action="?step=1"><label class='screen-reader-text' for='language'>Select a default language</label>
<select size='14' name='language' id='language'>
<option value="" lang="en" selected="selected" data-continue="Continue" data-installed="1">English (United States)</option>
<option value="af" lang="af" data-continue="Gaan voort">Afrikaans</option>
<option value="ar" lang="ar" data-continue="المتابعة">العربية</option>
<option value="ary" lang="ar" data-continue="المتابعة">العربية المغربية</option>
```

Test de connexion sur `web.cesi` : 
```html
[thomas@db ~]$ curl -L http://web.cesi:80
<!DOCTYPE html>
<html lang="en-US" xml:lang="en-US">
<head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow" />
        <title>WordPress &rsaquo; Installation</title>
        <link rel='stylesheet' id='dashicons-css'  href='http://10.99.99.11/wp-includes/css/dashicons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='buttons-css'  href='http://10.99.99.11/wp-includes/css/buttons.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='forms-css'  href='http://10.99.99.11/wp-admin/css/forms.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='l10n-css'  href='http://10.99.99.11/wp-admin/css/l10n.min.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='install-css'  href='http://10.99.99.11/wp-admin/css/install.min.css?ver=5.6' type='text/css' media='all' />
</head>
<body class="wp-core-ui language-chooser">
<p id="logo">WordPress</p>

        <form id="setup" method="post" action="?step=1"><label class='screen-reader-text' for='language'>Select a default language</label>
<select size='14' name='language' id='language'>
<option value="" lang="en" selected="selected" data-continue="Continue" data-installed="1">English (United States)</option>
<option value="af" lang="af" data-continue="Gaan voort">Afrikaans</option>
<option value="ar" lang="ar" data-continue="المتابعة">العربية</option>
<option value="ary" lang="ar" data-continue="المتابعة">العربية المغربية</option>
<option value="as" lang="as" data-continue="Continue">অসমীয়া</option>
<option value="az" lang="az" data-continue="Davam">Azərbaycan dili</option>
<option value="azb" lang="az" data-continue="Continue">گؤنئی آذربایجان</option>
<option value="bel" lang="be" data-continue="Працягнуць">Беларуская мова</option>
<option value="bg_BG" lang="bg" data-continue="Продължение">Български</option>
```

## IV. Un peu de sécu
### 1. fail2ban
---
Installation des paquets pour le fonctionnement de `fail2ban` : 
```
sudo yum install fail2ban-server fail2ban-firewalld
```
Création d'un fichier `sshd.local` afin de protéger le service SSH : 
```
sudo vim /etc/fail2ban/jail.d/sshd.local
[DEFAULT]
bantime = 120
ignoreip = 

[sshd]
enabled = true
```
Démarrage du service et vérification de l'état : 
```
sudo systemctl enable fail2ban
Created symlink from /etc/systemd/system/multi-user.target.wants/fail2ban.service to /usr/lib/systemd/system/fail2ban.service.

sudo systemctl start fail2ban

[thomas@rp ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```

Suivi des logs et test de ban :
```
sudo tail -f /var/log/fail2ban.log
2021-01-06 15:49:59,922 fail2ban.jail           [2956]: INFO    Creating new jail 'sshd'
2021-01-06 15:49:59,939 fail2ban.jail           [2956]: INFO    Jail 'sshd' uses systemd {}
2021-01-06 15:49:59,940 fail2ban.jail           [2956]: INFO    Initiated 'systemd' backend
2021-01-06 15:49:59,941 fail2ban.filter         [2956]: INFO      maxLines: 1
2021-01-06 15:49:59,941 fail2ban.filtersystemd  [2956]: INFO    [sshd] Added journal match for: '_SYSTEMD_UNIT=sshd.service + _COMM=sshd'
2021-01-06 15:49:59,961 fail2ban.filter         [2956]: INFO      maxRetry: 5
2021-01-06 15:49:59,961 fail2ban.filter         [2956]: INFO      encoding: UTF-8
2021-01-06 15:49:59,962 fail2ban.filter         [2956]: INFO      findtime: 600
2021-01-06 15:49:59,962 fail2ban.actions        [2956]: INFO      banTime: 120
2021-01-06 15:49:59,964 fail2ban.jail           [2956]: INFO    Jail 'sshd' started
2021-01-06 15:53:28,216 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:53:26
2021-01-06 15:53:41,115 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:53:41
2021-01-06 15:53:44,472 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:53:44
2021-01-06 15:53:53,273 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:53:53
2021-01-06 15:53:56,841 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:53:56
2021-01-06 15:53:57,105 fail2ban.actions        [2956]: NOTICE  [sshd] Ban 10.99.99.1
2021-01-06 15:54:00,661 fail2ban.filter         [2956]: INFO    [sshd] Found 10.99.99.1 - 2021-01-06 15:54:00
```

Tentative de brute force suivi d'un ban : 
```
PS C:\Users\thoml> ssh thomas@10.99.99.13
thomas@10.99.99.13's password:
Permission denied, please try again.
thomas@10.99.99.13's password:
Permission denied, please try again.
thomas@10.99.99.13's password:
thomas@10.99.99.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\thoml> ssh thomas@10.99.99.13
thomas@10.99.99.13's password:
Permission denied, please try again.
thomas@10.99.99.13's password:
Permission denied, please try again.
thomas@10.99.99.13's password:
thomas@10.99.99.13: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\thoml> ssh thomas@10.99.99.13
ssh: connect to host 10.99.99.13 port 22: Connection timed out
```

### 2. HTTPS
--- 

Ouverture du port 443 pour le `https` : 
```
sudo firewall-cmd --permanent --add-service=https

sudo firewall-cmd --reload

[thomas@rp ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens33 ens37
  sources:
  services: dhcpv6-client http https ssh
  ports: 443/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Modification de le configuration de nginx pour l'ajout de l'https et de la redirection :
```
sudo vim /etc/nginx/nginx.conf

events {}

http {
    server {
        listen       80;

        server_name web.cesi;
        return 301 https://10.99.99.13;

        location / {
            proxy_pass   http://10.99.99.11:80;
        }
    }
    server {
        listen       443 ssl http2;

        server_name web.cesi;

        location / {
            proxy_pass   http://10.99.99.11:443;
        }
##Certificates
ssl_certificate /etc/pki/tls/certs/web.cesi.crt;
ssl_certificate_key /etc/pki/tls/private/web.cesi.key;
    }

}
```

La redirection s'effectue bien curl nous prévient que le certificat n'est pas vérifié (il est possible de contourner ça avec l'option -k)
```
[thomas@web httpd]$ curl -L http://10.99.99.13
curl: (60) Peer's certificate issuer has been marked as not trusted by the user.
More details here: http://curl.haxx.se/docs/sslcerts.html

curl performs SSL certificate verification by default, using a "bundle"
 of Certificate Authority (CA) public keys (CA certs).
```

### 3. Monitoring
---

Installation de netdata avec le script : 

```
bash <(curl -Ss https://my-netdata.io/kickstart.sh)

sudo firewall-cmd --add-port=19999/tcp --permanent
sudo firewall-cmd --reload

[thomas@web ~]$ curl  http://10.99.99.11:19999
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" ...
...

```